package main

import (
	"encoding/csv"
	"fmt"
	"image/color"
	"os"
	"strconv"

	"gonum.org/v1/plot"
	"gonum.org/v1/plot/plotter"
	"gonum.org/v1/plot/vg"
)

type xy struct {
	x []float64
	y []float64
}

func NewXY(size int) xy {
	return xy{
		x: make([]float64, size),
		y: make([]float64, size),
	}
}

var _ plotter.XYer = (*xy)(nil)

func (d xy) Len() int {
	return len(d.x)
}

func (d xy) XY(i int) (float64, float64) {
	return d.x[i], d.y[i]
}

func main() {
	if len(os.Args) != 4 {
		fmt.Printf("usage: %s filename a b\n", os.Args[0])
		return
	}

	filename := os.Args[1]
	file, err := os.Open(filename)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	r := csv.NewReader(file)

	a, err := strconv.ParseFloat(os.Args[2], 64)
	if err != nil {
		panic(err)
	}

	b, err := strconv.ParseFloat(os.Args[3], 64)
	if err != nil {
		panic(err)
	}

	records, err := r.ReadAll()
	if err != nil {
		panic(err)
	}

	size := len(records)
	data := NewXY(size)
	for i, v := range records {
		if len(v) != 2 {
			panic("Expected 2 elements per line")
		}

		s, err := strconv.ParseFloat(v[0], 64)
		if err != nil {
			panic(err)
		}
		data.y[i] = s

		s, err = strconv.ParseFloat(v[1], 64)
		if err != nil {
			panic(err)
		}
		data.x[i] = s
	}

	line := plotter.NewFunction(func(x float64) float64 {
		return a*x + b
	})
	line.Color = color.RGBA{A: 255, B: 255}

	p := plot.New()

	plotter.DefaultLineStyle.Width = vg.Points(1)
	plotter.DefaultGlyphStyle.Radius = vg.Points(2)

	scatter, err := plotter.NewScatter(data)
	if err != nil {
		panic(err)
	}
	scatter.GlyphStyle.Color = color.RGBA{R: 255, B: 128, A: 255}

	p.Add(scatter, line)
	w, err := p.WriterTo(300, 300, "svg")
	if err != nil {
		panic(err)
	}

	_, err = w.WriteTo(os.Stdout)
	if err != nil {
		panic(err)
	}
}
